﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
using System.IO;

public class ItemDatabase : MonoBehaviour {

	private List<Item> database = new List<Item>();
	private JsonData jsonData;

	void Start() {
		jsonData = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/itemDatabase.json"));
		Debug.Log(jsonData[4]["sprite"]);
		ConstructDatabase();
	}

	private void ConstructDatabase() {
		for (int i = 0; i < jsonData.Count; i++) {
			database.Add(CreateItem(i));
		}
	}

	private Item CreateItem(int currentItem) {
		Item item = new Item(
			(int) jsonData[currentItem]["index"],
			(string) jsonData[currentItem]["name"],
			(string) jsonData[currentItem]["raw_name"],
			(string) jsonData[currentItem]["type"],
			(int) jsonData[currentItem]["max_quantity"],
			(string) jsonData[currentItem]["sprite"],
			(int) jsonData[currentItem]["value"],
			(int) 0); // 0 quantity

		return item;
	}

	public Item GetItemByIndex(int index) {
		for (int i =0; i < database.Count; i++) {
			if (database[i].index == index) {
				return database[i];
			}
		}

		return null;
	}
}
