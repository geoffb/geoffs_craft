﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateLandscape : MonoBehaviour {

	public static int height = 32;
	public static int depth = 32;
	public static int width = 32;
	public int heightScale = 20;
	public float smoothness = 25.0f;
	public int rockBottom = 0;

	public int cloudHeight = height-1;

	private BlockManager blockManager;

	// Use this for initialization
	void Start () {
		// Everytime we start the game it's generated the same - We need a seed different everytime
		int seed = (int) Network.time * 10;

		// Set manager
		blockManager = gameObject.GetComponent<BlockManager>();
		blockManager.SetWorld(width, height, depth);

		for (int z = 0; z < depth; z++) {
			for (int x = 0; x < width; x++) {
				// Range returns a float so need to cast it into int to be nice.
				// int y = (int) Random.Range(0, 10); 
				// The above line would make holes as we use randomness

				// Perlin noise function is used to make generated landscape
				int y = (int) (Mathf.PerlinNoise((x+seed)/smoothness, (z+seed)/smoothness) * heightScale);
				Vector3 blockPosition = new Vector3(x, y, z);

				CreateBlock(blockPosition, true);

				while (y > 0) {
					y--;
					blockPosition = new Vector3(x,y,z);
					CreateBlock(blockPosition, false);
				}
			}
		}

		createClouds();
		//createCaves();
	}

	void CreateBlock(Vector3 blockPosition, bool create) {
		blockManager.RegisterBlock(blockPosition, false);

		if (create) {
			blockManager.DrawBlock(blockPosition);
		}
	}

	private void createClouds() {
		// Whole square
		/*for (int i = 0; i < cloudHeight; i++) {
			for (int j = 0; j < cloudHeight; j++) {
				Vector3 newCloudBlockPosition = new Vector3(i, cloudHeight, j);
				CreateBlock(newCloudBlockPosition, true);
			}
		}*/
		int numClouds = 9;
		for (int i = 0; i < numClouds; i++) {
			int xpos = Random.Range(0, width);
			int zpos = Random.Range(0, depth);
			int cloudSize = 20;

			for (int j = 0; j < cloudSize; j++) {
				Vector3 cloudBlockPosition = new Vector3(xpos, cloudHeight, zpos);
				CreateBlock(cloudBlockPosition, true);
				xpos += Random.Range(-1, 2);
				zpos += Random.Range(-1, 2);

				if (xpos < 0 || xpos >= width) {
					xpos = width/2;
				}

				if (zpos < 0 || zpos >= depth) {
					zpos = depth/2;
				}
			}
		}
	}

	/*private void createCaves() {
		int numCaves = 10;
		int caveWidth = 5;
		int caveDepth = 5;

		for (int i = 0; i < numCaves; i++) {
			int xpos = Random.Range(0, width);
			int zpos = Random.Range(0, depth);

			Vector3 blockPos = new Vector3(xpos, 0, zpos);
			GameObject newDiamond = Instantiate(diamondBlock, blockPos, Quaternion.identity);
			newDiamond.transform.SetParent(World.transform);

			for (int j = 0; j < caveWidth; j++) {
				xpos += 1;
				for (int k = 0; k < caveDepth; k++) {
					zpos += 1;
					for (int l = 0; l < 5; l++) {
						blockPos = new Vector3(xpos, l, zpos);
						newDiamond = Instantiate(diamondBlock, blockPos, Quaternion.identity);
						newDiamond.transform.SetParent(World.transform);
					}
				}
			}
		}
	}*/
}
