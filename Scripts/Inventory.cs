﻿using UnityEngine; // GameObject
using System.Collections.Generic; // List
using UnityEngine.UI;
using System.IO;

public class Inventory : MonoBehaviour {
	public Item[] items;
	public int maxItems = 8;
	public bool active = false;
	public GameObject slot;

	private GameObject theInventory;
	private GameObject inventoryPanel;
	private ItemDatabase database;
	private int currentItems = 0;

	private string iconsPath = "Sprites/Icons/";
	private Sprite itemIcon;

	void Start() {
		theInventory = GameObject.FindGameObjectWithTag("Inventory");
		inventoryPanel = GameObject.FindGameObjectWithTag("SlotsPanel");
		database = GetDatabase();

		items = new Item[maxItems];

		Hide();
	}

	public ItemDatabase GetDatabase() {
		GameObject databaseGameObject = GameObject.FindGameObjectWithTag("Item Database");
		return databaseGameObject.GetComponent<ItemDatabase>();
	}

	public void Show() {
		theInventory.SetActive(true);
		active = true;

		// Stop moving camera and give cursor
	}

	public void Hide() {
		theInventory.SetActive(false);
		active = false;

		// Start camera
	}

	public void Add(int index, int quantity) {
		// Check to see if we have a valid index
		if (index == -1) {
			Debug.Log("No item added to inventory");
			return;
		}

		// If we can add an item
		if (currentItems <= maxItems) {
			int itemKey = ItemInInventory(index);

			// If the item is in the inventory: update it
			if (itemKey >= 0) {
				GameObject slot = inventoryPanel.transform.FindChild("Slot_" + itemKey).gameObject;
				GameObject text = slot.transform.FindChild("slot_text").gameObject;

				items[itemKey].addItem(quantity);

				text.GetComponent<Text>().text = "" + items[itemKey].quantity;
			}
			// Otherwise we create it
			else {
				// Get item from database
				Item newItem = GetItem(index);

				// Update quantity
				newItem.addItem(quantity);

				// Update UI
				AddItemToUI(newItem);

				// Add it to the inventory array
				items[currentItems] = newItem;

				// Update how many objects we have
				currentItems++;
			}
		}
	}

	private void AddItemToUI(Item item) {
		GameObject newSlot = Instantiate(slot);
		newSlot.transform.SetParent(inventoryPanel.transform, false);

		newSlot.name = "Slot_" + currentItems;

		GameObject text = newSlot.transform.FindChild("slot_text").gameObject;
		text.GetComponent<Text>().text = "" + item.quantity;

		itemIcon  = Resources.Load(iconsPath + item.sprite, typeof(Sprite)) as Sprite;
		GameObject image = newSlot.transform.FindChild("slot_image").gameObject;
		image.GetComponent<Image>().sprite = itemIcon;
	}

	public Item GetItem(int index) {
		return database.GetItemByIndex(index);
	}

	public int ItemInInventory(int index) {
		for (int i = 0; i < items.Length; i++) {
			if (items[i] != null) {
				if (items[i].index == index) {
					return i;
				}
			}
		}

		return -1;
	}
}