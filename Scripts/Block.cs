﻿public class Block {
	public int type;
	public bool isVisible;

	public Block(int blockType, bool blockVisibility) {
		type = blockType;
		isVisible = blockVisibility;
	}
}
