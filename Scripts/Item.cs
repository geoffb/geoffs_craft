﻿public class Item {	
	public int index;
	public string name;
	public string rawName;
	public string type;
	public int maxQuantity;
	public string sprite;
	public int value;
	public int quantity;

	public Item(int itemIndex, string itemName, string itemRawName, string itemType, int itemMaxQuantity, string itemSprite, int itemValue, int itemQuantity) {
		this.index = itemIndex;
		this.name = itemName;
		this.rawName = itemRawName;
		this.type = itemType;
		this.maxQuantity = itemMaxQuantity;
		this.sprite = itemSprite;
		this.value = itemValue;
		this.quantity = itemQuantity;
	}

	public void addItem(int addedQuantity) {
		this.quantity = this.quantity + addedQuantity;

		if (this.quantity > this.maxQuantity) {
			this.quantity = this.maxQuantity;
		}
	}

	public void removeItem(int removedQuantity) {
		this.quantity = this.quantity - removedQuantity;
	}
}