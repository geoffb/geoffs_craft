﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour {

	private GameObject playerObject;
	private Player thePlayer;
	private GameObject worldObject;
	private BlockManager blockManager;

	void Start () {
		// Player
		playerObject = GameObject.FindGameObjectWithTag("Player");
		thePlayer = playerObject.GetComponent<Player>();

		// Block manager
		worldObject = GameObject.FindGameObjectWithTag("World");
		blockManager = worldObject.GetComponent<BlockManager>();
	}
	
	void Update () {
		/////////////////
		// KEYS
		//
		bool shiftDown = false;

		// SET
		if (Input.GetKey(KeyCode.LeftShift)) {
			shiftDown = true;
		}

		// WATCHER

		// I -> open/close inventory
		if (Input.GetKeyDown(KeyCode.I)) {
			if (thePlayer.inventory.active) {
				thePlayer.inventory.Hide();
				thePlayer.crosshair.SetActive(true);
			}
			else {
				thePlayer.inventory.Show();
				thePlayer.crosshair.SetActive(false);
			}
		}

		/////////////////
		// CLICKS
		//
		bool leftClick = false;

		// SET
		if (Input.GetMouseButtonDown(0)) {
			leftClick = true;
		}

		// WATCHER
		if (shiftDown && leftClick) {
			blockManager.AddBlock();
		}
		else if (leftClick) {
			if (thePlayer.inventory.active) {
				// Click in inventory

				// Give cursor?

				Debug.Log("Inventory active");
			}
			else {
				// Block or anything else.

				// Need a function to check which target (move hit and check tag or something)
				int blockType = blockManager.DestroyBlock(); // Returns -1 if no object

				// Should only pass index number of object of raw_name and quantity
				thePlayer.inventory.Add(blockType, 1);
			}
		}
		else {
			//Debug.Log(shiftDown + " = " + leftClick);
		}
	}
}
