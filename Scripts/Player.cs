﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	public static int numItemInventory = 8;
	public Inventory inventory;
	public GameObject crosshair;
	public bool canMove;

	// Use this for initialization
	void Start () {
		inventory = gameObject.GetComponent<Inventory>();
		crosshair = GameObject.FindGameObjectWithTag("Crosshair");

		canMove = true;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void FreezePlayer(bool frozen) {
		if (frozen) {
			canMove = true;

		}
		else {
			canMove = false;
		}
	}
}
