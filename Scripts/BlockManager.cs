﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockManager : MonoBehaviour {
	private GameObject World;

	// Create a matrix (3 dimentional [,,] array) of the world
	public Block[,,] worldBlocks;
	public float hitDistance = 8.0f;
	public int rockBottom = 0;

	// All different blocks
	public GameObject grassBlock;
	public GameObject sandBlock;
	public GameObject snowBlock;
	public GameObject diamondBlock;
	public GameObject cloudBlock;

	// Height of blocs
	public int snowHeight = 15;
	public int sandHeight = 5;
	public int cloudHeight;

	void Start() {
		SetWorld();
	}

	public void SetWorld(int width, int height, int depth) {
		// Create a matrix (3 dimentional [,,] array) of the world
		worldBlocks = new Block[width, height, depth];

		cloudHeight = height - 1;
	}

	public void UnregisterBlock(Vector3 blockPosition) {
		worldBlocks[(int) blockPosition.x, (int) blockPosition.y, (int) blockPosition.z] = null;
	}

	public void RegisterBlock(Vector3 blockPosition, bool created) {
		int blockTypeNumber = GetBlockTypeNumber((int) blockPosition.y);
		Block newBlock = new Block(blockTypeNumber, created);

		worldBlocks[(int)blockPosition.x, (int)blockPosition.y, (int)blockPosition.z] = newBlock;
	}

	public int GetBlockTypeNumber(int yPosition) {
		// 0 Rock Bottom
		if (yPosition == rockBottom) {
			return 0;
		}
		// 1 -> 5 : Sand
		else if (yPosition > rockBottom && yPosition <= sandHeight) {
			return 1;
		}
		// 6 -> 15 : Grass
		else if (yPosition > sandHeight && yPosition <= snowHeight) {
			return 2;
		}
		// 16 -> 30 : Snow
		else if (yPosition > snowHeight && yPosition < cloudHeight) {
			return 3;
		}
		// 31 : Cloud
		else {
			return 4;
		}
	}

	public GameObject GetBlockObject(int typeNumber) {
		switch (typeNumber) {
			case 0:
				return sandBlock;

			case 1:
				return sandBlock;

			case 2:
				return grassBlock;
			
			case 3:
				return snowBlock;

			case 4:
				return cloudBlock;

			default:
				return grassBlock;
		}
	}

	public Block GetBlock(int x, int y, int z) {
		return worldBlocks[x, y, z];
	}

	private void DrawNeighbours(Vector3 lastPosition) {
		for (int x = -1; x <= 1; x++) {
			for (int y = -1; y <= 1; y++) {
				for (int z = -1; z <= 1; z++) {
					if (!(x == 0 && y == 0 && z == 0)) {
						Vector3 neighbour = new Vector3(lastPosition.x+x, lastPosition.y+y, lastPosition.z+z);
						DrawBlock(neighbour);
					}
				}
			}
		}
	}

	public void DrawBlock(Vector3 newBlockPosition) {
		if (newBlockPosition.x < rockBottom || newBlockPosition.x >= worldBlocks.GetLength(0) ||
			newBlockPosition.y < rockBottom || newBlockPosition.y >= worldBlocks.GetLength(1) ||
			newBlockPosition.z < rockBottom || newBlockPosition.z >= worldBlocks.GetLength(2)) {
			Debug.Log("Out of map!");
			return;
		}

		Block block = GetBlock((int) newBlockPosition.x, (int) newBlockPosition.y, (int) newBlockPosition.z);

		if (block == null) {
			//Debug.Log("Block not set in worldBlocks, can not create");
			return;
		}

		// If not in scene
		if (!block.isVisible) {
			GameObject blockGameObject = GetBlockObject(block.type);
			worldBlocks[(int) newBlockPosition.x, (int) newBlockPosition.y, (int) newBlockPosition.z].isVisible = true;

			InstantiateBlock(blockGameObject, newBlockPosition);
		}
	}

	public void InstantiateBlock(GameObject block, Vector3 position) {
		GameObject newBlock = Instantiate(block, position, Quaternion.identity);

		if (World == null) {
			SetWorld();
		}

		newBlock.transform.SetParent(World.transform, false);
	}

	public int DestroyBlock() {
		int blockType = -1; // Set -1 for return, if no block hit
		RaycastHit hit;
		Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width/2.0f, Screen.height/2.0f, 0));

		if (Physics.Raycast(ray, out hit, hitDistance)) {
			Vector3 blockPosition = hit.transform.position;

			if ((int)blockPosition.y == rockBottom) {
				Debug.Log("Rock Bottom!");
				return blockType;
			}

			blockType = GetBlockTypeNumber((int) blockPosition.y);
			UnregisterBlock(blockPosition);
			DrawNeighbours(blockPosition);
			Destroy(hit.transform.gameObject);
		}

		return blockType;
	}

	public void AddBlock() {
		RaycastHit hit;
		Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width/2.0f, Screen.height/2.0f, 0));

		if (Physics.Raycast(ray, out hit, hitDistance)) {
			Vector3 blockPosition = hit.transform.position;
			blockPosition.y += 1;

			RegisterBlock(blockPosition, false);
			DrawBlock(blockPosition);
		}
	}

	private void SetWorld() {
		World = GameObject.FindGameObjectWithTag("World");
	}
}
